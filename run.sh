echo -e "Updating and Upgrading system..."
sudo apt-get update && apt-get upgrade -y

echo -e "Installing packages..."
sudo apt-get install docker.io -y 

echo -e "Post install docker..."
sudo groupadd docker
sudo usermod -aG docker $USER
newgrp docker

mkdir -p ~/builds/{feedstock,home,dashboard}

docker pull nginx:alpine

